from django.db import models
from django.urls import reverse

class AutoVO(models.Model):
    import_href = models.CharField(max_length=150, unique=True, null=True)
    vin = models.CharField(max_length=17, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    name = models.CharField(max_length=150)
    employee_number = models.CharField(max_length=100, unique=True)

class Service(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=200)
    date = models.DateTimeField(null=True)
    reason = models.TextField()

    technician = models.ForeignKey(
        Technician,
        related_name="services",
        on_delete=models.CASCADE,
        null=True,
    )

    auto = models.ForeignKey(
        AutoVO,
        related_name="services",
        on_delete=models.CASCADE,
        null=True,
    )

    vip = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("detail_services", kwargs={"pk": self.pk})

    def __str__(self):
        return self.vin

    def finish(self):
        self.finished = True
        self.save()

    class Meta:
        ordering = ("vin", "customer_name", "date", "technician", "reason", "auto")
