from django.urls import path
from .views import list_services, detail_service, list_technicians, detail_technician

urlpatterns = [
    path("services/", list_services, name="list_services"),
    path("services/<int:pk>/", detail_service, name="detail_service"),
    path("technicians/", list_technicians, name="list_technicians"),
    path("technicians/<int:pk>/", detail_technician, name="detail_technician"),
]
