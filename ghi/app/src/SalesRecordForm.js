import React, { useEffect, useState } from 'react';

function SalesRecordsForm() {

    const [salesPerson, setSalesRep] = useState("");
    const [salesPeople, setSalesReps] = useState([]);
    const [customer, setCustomer] = useState("");
    const [customers, setCustomers] = useState([]);
    const [automobile, setAutomobile] = useState("");
    const [automobiles, setAutomobiles] = useState([]);
    const [price, setPrice] = useState("");


    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesRep(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.sales_person = salesPerson;
        data.customer = customer;
        data.automobile = automobile;
        data.price = price;

        const recordUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const recordResponse = await fetch(recordUrl, fetchConfig);
        if (recordResponse.ok) {
            const newRecord = await recordResponse.json();

            setSalesRep('');
            setCustomer('');
            setAutomobile('');
            setPrice('');
        }
    }

    const fetchData = async () => {
        const salesPersonUrl = 'http://localhost:8090/api/employees/';
        const salesPersonResponse = await fetch(salesPersonUrl);
        const customerUrl = 'http://localhost:8090/api/customers/';
        const customerResponse = await fetch(customerUrl);
        const automobileUrl = 'http://localhost:8090/api/automobiles/';
        const automobileResponse = await fetch(automobileUrl);
        if (salesPersonResponse.ok && customerResponse.ok && automobileResponse.ok) {
            const salesPersonData = await salesPersonResponse.json();
            const customerData = await customerResponse.json();
            const automobileData = await automobileResponse.json();
            setSalesReps(salesPersonData.sales_people);
            setCustomers(customerData.potential_customers);
            setAutomobiles(automobileData.automobileVO);
        }
    }


    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-salesRecord-form">
                    <div className="mb-3">
                        <select onChange={handleSalesPersonChange} value={salesPerson} required id="salesPerson" name="salesPerson" className="form-select">
                            <option value="">Choose a Sales Person</option>
                            {salesPeople?.map(salesPerson => {
                                return(
                                    <option key={salesPerson.id} value={salesPerson.id}>
                                        {salesPerson.name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleCustomerChange} value={customer} required id="customer" name="customer" className="form-select">
                            <option value="">Choose a Customer</option>
                            {customers?.map(customer => {
                                return(
                                    <option key={customer.id} value={customer.id}>
                                        {customer.name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleAutomobileChange} value={automobile} required id="automobile" name="automobile" className="form-select">
                            <option value="">Choose an Automobile</option>
                            {automobiles.map(automobile => {
                                return(
                                    <option key={automobile.vin} value={automobile.vin}>
                                        {automobile.vin}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePriceChange} value={price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                        <label htmlFor="price">Price</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
            </div>
        )
    }

export default SalesRecordsForm;
