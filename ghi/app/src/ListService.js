
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


const ServicesList = () => {

    const [services, setServices] = useState([]);

    const fetchServices = async () => {
        const serviceUrl = 'http://localhost:8080/api/services/'
        const serviceResponse = await fetch(serviceUrl);

        if (serviceResponse.ok) {
            const serviceData = await serviceResponse.json();
            setServices(serviceData.services);
        }
    }

    useEffect(() => {
        fetchServices();
    }, []);

    const finishService = async (id) => {
        const serviceDetailUrl = `http://localhost:8080/api/services/${id}/`;
        const fetchConfig = {
            method: 'put',
            body: JSON.stringify({ "finished": true }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const finishResponse = await fetch(serviceDetailUrl, fetchConfig);
        if (finishResponse.ok) {
            const idx = services.findIndex((service) => service.id === id);
            const updatedServices = [...services];
            updatedServices[idx] = {...updatedServices[idx], finished:true };
            setServices(updatedServices);
        }
    };


    const cancelService = async (id) => {
        const serviceDetailUrl = `http://localhost:8080/api/services/${id}/`;
        const fetchConfig = {
            method: 'delete',
            body: JSON.stringify(id),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const cancelResponse = await fetch(serviceDetailUrl, fetchConfig);
        if (cancelResponse.ok) {
            fetchServices();
        }
    };

    return(
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date | Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>VIP</th>
                        <th>Finish</th>
                        <th>Cancel</th>
                    </tr>
                </thead>
                <tbody>
                {services.map((service) => {
                    if (!service.finished) {
                    const date = new Date(service.date);
                    const localDate = date.toLocaleString();
                        return (
                            <tr key={service.id}>
                            <td>{ service.vin }</td>
                            <td>{ service.customer_name }</td>
                            <td>{ localDate }</td>
                            <td>Name: { service.technician.name } ID: { service.technician.employee_number }</td>
                            <td>{ service.reason }</td>
                            <td>{ service.vip ? "Yes" : "No"}</td>
                            <td>
                                <button type="button" className="btn btn-outline-success" onClick={(serv) => finishService(service.id)}>
                                    Finish
                                </button>
                            </td>
                            <td>
                                <button type="button" className="btn btn-outline-danger" onClick={(serv) => cancelService(service.id)}>
                                    Cancel
                                </button>
                            </td>
                        </tr>
                    );
                    }
                })}
                </tbody>
            </table>
            <Link to="/service/history/" className="btn btn-outline-info">Service Appointment History</Link>
        </div>
    );
}

export default ServicesList;
