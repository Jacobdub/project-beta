import React, { useState } from 'react';

function TechnicianForm() {

    const [name, setName] = useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');
    const [submitted, setSubmitted] = useState(false);
    const [error, setError] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.employee_number = employeeNumber;

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            },
        };

        const technicianResponse = await fetch(technicianUrl, fetchConfig);
        if (technicianResponse.ok) {
            const newTechnician = await technicianResponse.json();


            setName('');
            setEmployeeNumber('');
            setSubmitted(true);
            setError('');
        } else if (technicianResponse.status === 400) {
            setError(`Employee Number: ${employeeNumber} already exists`)
        } else {
            setError('Error, cannot add technician');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeNumberChange} value={employeeNumber} placeholder="Employee Number" required type="text" name="employee-number" id="employee-number" className="form-control" />
                        <label htmlFor="employee-number">Employee Number</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                    {error && (
                        <div className="alert alert-warning mt-3">
                            {error}
                        </div>
                    )}
                    {submitted && (
                        <div className="alert alert-success mt-3">
                            New Technician Added
                        </div>
                    )}
                </div>
            </div>
            </div>
    )
}

export default TechnicianForm;
