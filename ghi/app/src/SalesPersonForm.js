import React, { useState } from 'react';

function SalesPersonForm() {

    const [name, setName] = useState('');
    const [employeeID, setEmployeeID] = useState('');


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.employee_id = employeeID

        const salesPersonUrl = 'http://localhost:8090/api/employees/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const salesPersonResponse = await fetch(salesPersonUrl, fetchConfig);
        if (salesPersonResponse.ok) {
            const newSalesPerson = await salesPersonResponse.json();

            setName('');
            setEmployeeID('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Sales Person</h1>
                    <form onSubmit={handleSubmit} id="create-salesPerson-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeIDChange} value={employeeID} placeholder="EmployeeID" required type="text" name="employeeID" id="employeeID" className="form-control" />
                        <label htmlFor="name">Employee id</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
            </div>
        )
    }

export default SalesPersonForm
