import React, { useState, useEffect } from 'react';

const ServicesHistory = () => {

    const [services, setServices] = useState([]);
    const [filtered, setFiltered] = useState([]);
    const [query, setQuery] = useState("");
    const [submitted, setSubmitted] = useState(false);


    useEffect(() => {
        const fetchConfig = async () => {
            const serviceUrl = 'http://localhost:8080/api/services/';
            const serviceResponse = await fetch(serviceUrl);

            if (serviceResponse.ok) {
                const serviceData = await serviceResponse.json();
                setServices(serviceData.services);
            }
        };

        fetchConfig();
    }, []);

    const handleSearchChange = async (event) => {
        const search = services.filter((service) =>
            service.vin.includes(query)
        );
        setFiltered(search);
        setSubmitted(true);
    };

    return(
        <div>
            <div className="mt-3 mb-3">
                <input
                    type="text"
                    className="btn btn-outline-success"
                    value={query}
                    id="form1"
                    onChange={(serv) => setQuery(serv.target.value)}
                />
                <button onClick={handleSearchChange} type="button" className="btn btn-outline-success">
                    Search
                </button>
            </div>
            <h1>Service History</h1>
            {filtered.length > 0 && (
                <table className="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date | Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                {filtered.map(service => {
                    const date = new Date(service.date);
                    const localDate = date.toLocaleString();
                    return (
                        <tr key={service.id}>
                        <td>{ service.vin }</td>
                        <td>{ service.customer_name }</td>
                        <td>{ localDate }</td>
                        <td>Name: { service.technician.name } ID: { service.technician.employee_number }</td>
                        <td>{ service.reason }</td>
                        <td>{ service.finished ? "Yes" : "No"}</td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
            )}
            {submitted && filtered.length === 0 && (
                <div className="alert alert-warning" role="alert">
                    <p>Unavailable VIN</p>
                </div>
            )}
        </div>
    );
}

export default ServicesHistory;
