import { Link } from "react-router-dom";

function SalesList(props) {
    return(
        <div>
        <h1>All Sales</h1>
        <table className="table table-dark table-striped table-hover">
            <thead>
                <tr>
                    <th>Sales Person</th>
                    <th>Employee Number</th>
                    <th>Customer</th>
                    <th>Automobile VIN</th>
                    <th>Price of Sale</th>
                </tr>
            </thead>
            <tbody>
            {props.sales_records.map(sales_records => {
                return (
                    <tr key={sales_records.id}>
                        <td>{ sales_records.sales_person.name }</td>
                        <td>{ sales_records.sales_person.employee_id }</td>
                        <td>{ sales_records.customer.name }</td>
                        <td>{ sales_records.automobile.vin }</td>
                        <td>{ sales_records.price }</td>
                    </tr>
                );
            })}
            </tbody>
        </table>
        <Link to="/record/employee/" className="btn btn-outline-info">Sales Records by Sales Person</Link>
        </div>
    );
}

export default SalesList;
