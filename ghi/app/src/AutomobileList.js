
function AutomobilesList(props) {
    return(
        <table className="table table-dark table-striped table-hover">
            <thead>
                <tr>
                    <th>Color</th>
                    <th>Year</th>
                    <th>VIN</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
            {props.autos.map(auto => {
                return (
                    <tr key={auto.id}>
                    <td>{ auto.color }</td>
                    <td>{ auto.year }</td>
                    <td>{ auto.vin }</td>
                    <td>{ auto.model.name }</td>
                    <td>{ auto.model.manufacturer.name }</td>
                    </tr>
                );
            })}
            </tbody>
        </table>
    );
}

export default AutomobilesList;
