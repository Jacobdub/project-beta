
function VehicleListModels(props) {
    return(
        <table className="table table-dark table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Picture Url</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
            {props.models.map(model => {
                return (
                    <tr key={model.id}>
                    <td>{ model.name }</td>
                    <td><img src={ model.picture_url } alt="..." width="80" height="50" /></td>
                    <td>{ model.manufacturer.name }</td>
                    </tr>
                );
            })}
            </tbody>
        </table>
    );
}

export default VehicleListModels;
