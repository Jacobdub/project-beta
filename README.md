# CarCar

Team:

* Youngsuk Park - Services microservice
* Jacob Williams - Sales microservice

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

Step-by-step instruction to run the project:
docker volume create beta-data
docker-compose build
docker-compose up

Diagram of the project:
Refer to CarCarProjectDiagram.png above docker-compose.yml

Define URL & Ports:
For services port, we're using 8080:8000. Meaning that we can pull up this information on Insomnia by referring to it as localhost:8080/... The Inventory port would be 8100. The urls to get to the services on the web browser would be 3000 + the specified url in the React front-end stuff. We will be creating urls.py for our backend along with models and views to get a list or details of a service(s) also with the option to create and delete.

CRUD Route Documentation:
In our Insomnia, we'll be sending get responses for list and detailed views as localhost:8080 with other urls tied to it from urls.py. For create and delete, we'll be sending a JSON body with the info that is required such as the VIN or manufacturer. As our output or response, we'll be getting back an object with the information that we sent in the JSON body.

Identification of Value Objects:
We believe that VIN is an ENTITY OBJECT. However, we believe objects such as customers, color of the model, the price of the vehicle are possible targets of value objects. A VIN cannot change and is tied to its specific identity while customers (the person who drives the vehicle) could change depending on the sales of the car. A person might decide to sell their car after driving it, making it so that the owner of the vehicle is changed. The price would also change over time equaling to less values for most cars as time elapses. The customers/consumers who have purchased the vehicle might decide to change the color of the vehicle as well. Therefore, we believe that these three are examples of value objects.


## Sales microservice

Explain your models and integration with the inventory
microservice, here.

Step-by-step instruction to run the project:
docker volume create beta-data
docker-compose build
docker-compose up

Diagram of the project:
Refer to CarCarProjectDiagram.png above docker-compose.yml

Define URL & Ports:
For sales port, we're using 8090:8000. Meaning that we can pull up this information on Insomnia by referring to it as localhost:8090/... The Inventory port would be 8100. The urls to get to the services on the web browser would be 3000 + the specified url in the React front-end stuff. We will be creating urls.py for our backend along with models and views to get a list or details of a sale(s) also with the option to create and delete.

CRUD Route Documentation:
In our Insomnia, we'll be sending get responses for list and detailed views as localhost:8090 with other urls tied to it from urls.py. For create and delete, we'll be sending a JSON body with the info that is required such as the VIN or manufacturer. As our output or response, we'll be getting back an object with the information that we sent in the JSON body.

Identification of Value Objects:
We believe that VIN is an ENTITY OBJECT. However, we believe objects such as customers, color of the model, the price of the vehicle are possible targets of value objects. A VIN cannot change and is tied to its specific identity while customers (the person who drives the vehicle) could change depending on the sales of the car. A person might decide to sell their car after driving it, making it so that the owner of the vehicle is changed. The price would also change over time equaling to less values for most cars as time elapses. The customers/consumers who have purchased the vehicle might decide to change the color of the vehicle as well. Therefore, we believe that these three are examples of value objects.
